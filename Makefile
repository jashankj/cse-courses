curl	:= curl --location --compressed
rdfproc	:= rdfproc --storage sqlite

courses.db: rdf.ttl rdfs.ttl owl.ttl foaf.rdf s.ttl dct.ttl handbook.ttl courses.ttl offerings.ttl
	${rdfproc} --new $@ parse rdf.ttl       turtle
	${rdfproc}       $@ parse rdfs.ttl      turtle
	${rdfproc}       $@ parse owl.ttl       turtle
	${rdfproc}       $@ parse foaf.rdf      rdfxml
#	${rdfproc}       $@ parse s.ttl         turtle
	${rdfproc}       $@ parse dct.ttl       turtle
	${rdfproc}       $@ parse handbook.ttl  turtle
	${rdfproc}       $@ parse courses.ttl   turtle
	${rdfproc}       $@ parse offerings.ttl turtle
	${rdfproc}       $@ size

rdf.ttl:
	${curl} -o $@ http://www.w3.org/1999/02/22-rdf-syntax-ns
rdfs.ttl:
	${curl} -o $@ http://www.w3.org/2000/01/rdf-schema
owl.ttl:
	${curl} -o $@ http://www.w3.org/2002/07/owl
foaf.rdf:
	${curl} -o $@ http://xmlns.com/foaf/spec/index.rdf
s.ttl:
	${curl} -o $@ https://schema.org/version/10.0/schemaorg-current-http.ttl
	# https://schema.org/version/latest/schemaorg-current-http.ttl
dct.ttl:
	${curl} -o $@ https://dublincore.org/specifications/dublin-core/dcmi-terms/dublin_core_terms.ttl
	# http://purl.org/dc/terms/
