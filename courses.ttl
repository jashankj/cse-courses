#
# An incomplete list of courses offered at
# the School of Computer Science and Engineering
# of the Faculty of Engineering
# at the University of New South Wales, Sydney.
#
# The eventual successor to <https://wiki.jashankj.space/CSE/Courses/>
#
# 2020-10-26	Jashank Jeremy <jashank.jeremy@unsw.edu.au>
#

# @base <https://www.handbook.unsw.edu.au/> .
@prefix rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd:  <http://www.w3.org/2001/XMLSchema#> .
@prefix owl:  <http://www.w3.org/2002/07/owl#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix s:    <http://schema.org/> .
@prefix ps:   <http://pending.schema.org/> .
@prefix dct:  <http://purl.org/dc/terms/> .
@prefix unsw: <https://gitlab.com/jashankj/cse-courses/-/raw/trunk/handbook.ttl#> .
@prefix c:    <https://gitlab.com/jashankj/cse-courses/-/raw/trunk/courses.ttl#> .

########################################################################

c:COMP1000
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1000" ;
    s:name "Introduction to Web, Spreadsheets, and Databases" ;
    s:url <https://www.cse.unsw.edu.au/~cs1000/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:successorOf c:COMP0011, c:GENE8000, c:COMP1001;
    s:abstract """
        World wide web (WWW) dependent activities have become an
        essential part of our existence and yet many of us do not know
        much about how they work.  In the first part of this course, we
        introduce the abstract features of the internet and the software
        that makes it so powerful.  Knowledge about the architecture of
        the internet, world wide web, browsers, search engines,
        e-commerce, security, etc.  will be helpful in using the
        internet more effectively as well as becoming aware of the
        several pitfalls associated with this modern technology.

        Spreadsheets and databases are two of the most commonly used and
        powerful computer tools yet they are often poorly utilised and
        the reasons for using one rather than the other are poorly
        understood.  Many people purchase Microsoft Office which comes
        bundled with Microsoft Excel and Microsoft Access - a
        spreadsheet and a database application - yet while many people
        have some familiarity with Microsoft Excel, Microsoft Access is
        rarely utilised.  This course aims to explain in straightforward
        terms the concepts underlying both of these powerful pieces of
        software so that students can exploit them effectively for both
        their studies and future careers.  We will investigate how to
        design and implement effective spreadsheet and database
        applications.  Students should also be able to transfer these
        skills to other similar spreadsheet and database packages.

        Lab access will be provided, however students will be expected
        to have personal copies of Microsoft Excel and Microsoft Access
        on their own computers.
    """@en ;
    .

c:COMP0011
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP0011" ;
    s:name "Fundamentals of Computing" ;
    s:url <https://www.cse.unsw.edu.au/~cs0011/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:succeededBy c:COMP1000 ; # replaced in 13s1
    s:coursePrerequisites
        [ unsw:not c:GENE8000 ] ;
    s:abstract """
        This course forms part of the Diploma of Science, Engineering
        and Technology - Dip(SET) - program.  This program provides
        opportunities for general broadening of the science skill base
        of students as well as for strengthening their academic literacy
        and skills in critical thinking and analysis.  In particular,
        this course extends students' knowledge of computing and
        information technologies to the level required by programs in
        the Faculty of Engineering and the Faculty of Science.  It is
        well suited to students who have not studied computing and
        information technology courses beyond year 10 high-school level.

        Course topics include: introduction to computer systems,
        researching a topic using the world-wide-web and electronic
        databases, structuring electronic documents, structuring
        presentations, publishing information available on the
        world-wide-web, analysing data using spreadsheets and databases,
        internet technologies, internet concerns, and current issues in
        information technology.

        Important Information Students who are not enrolled in the
        DipSET [7015] should consult their Program Office before
        enrolling in this course.

        Students who complete COMP0011, and later transfer to a bachelor
        degree program in the School of Computer Science and
        Engineering, will not receive credit for this course.
    """@en ;
    .

c:GENE8000
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "GENE8000" ;
    s:name "Spreadsheet and Databane Applications" ;
    s:alternateName "Spreadsheet & Database Applns" ;
    s:url <https://www.cse.unsw.edu.au/~ge8000/> ;
    ps:numberOfCredits 3 ;
    unsw:career unsw:UGRD ;
    s:succeededBy c:COMP1000 ; # replaced 13s1?
    s:abstract """
        Spreadsheets and databases are two of the most commonly used and
        powerful computer tools yet they are often poorly utilised and
        the reasons for using one rather than the other are poorly
        understood.  Many people purchase Microsoft Office which comes
        bundled with Microsoft Excel and Microsoft Access --- a
        spreadsheet and a database application --- yet while many people
        have some familiarity with Microsoft Excel, Microsoft Access is
        rarely utilised.  This course aims to explain in straightforward
        terms the concepts underlying both of these powerful pieces of
        software so that students can exploit them effectively for both
        their studies and future careers.  It will investigate how to
        design and implement effective spreadsheet and database
        applications.  Students should also be able to transfer these
        skills to other, similar spreadsheet and database packages.
        Students will be expected to have personal copies of Microsoft
        Excel and Microsoft Access on their own computers.
    """@en ;
    .

c:COMP1001
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1001" ;
    s:name "Introduction to Computing" ;
    s:url <https://www.cse.unsw.edu.au/~cs1001/> ;
    ps:numberOfCredits 6 ;
    s:succeededBy c:COMP1000 ;
    s:abstract """
        Introductory concepts and basic skills training for competence
        with personal computers.

        Vanished in 2004 ... looks like what COMP1000 is now.
    """@en ;
    .

########################################################################

c:COMP1511
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1511" ;
    s:name "Programming Fundamentals" ;
    s:alternateName "Introduction to Programming" ;
    s:url <https://www.cse.unsw.edu.au/~cs1511/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:successorOf c:COMP1917 ;
    s:coursePrerequisites
        [ unsw:not
            [ unsw:or c:COMP1911, c:COMP1921, c:COMP1917, c:DPST1091 ] ] ;
    s:abstract """
        An introduction to problem-solving via programming, which aims
        to have students develop proficiency in using a high level
        programming language.  Topics: algorithms, program structures
        (statements, sequence, selection, iteration, functions), data
        types (numeric, character), data structures (arrays, tuples,
        pointers, lists), storage structures (memory, addresses),
        introduction to analysis of algorithms, testing, code quality,
        teamwork, and reflective practice.  The course includes
        extensive practical work in labs and programming projects.

        Additional Information: This course should be taken by all CSE
        majors, and any other students who have an interest in computing
        or who wish to be extended.  It does not require any prior
        computing knowledge or experience.

        COMP1511 leads on to COMP1521, COMP1531, COMP2511 and COMP2521,
        which form the core of the study of computing at UNSW and which
        are pre-requisites for the full range of further computing
        courses.

        Due to overlapping material, students who complete COMP1511 may
        not also enrol in COMP1911 or COMP1921.
    """@en ;
    .

c:COMP1917
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1917" ;
    s:name "Computing 1A" ;
    s:url <https://www.cse.unsw.edu.au/~cs1917/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:succeededBy c:COMP1511 ;
    s:coursePrerequisites
        [ unsw:not c:COMP1921 ] ;
    s:abstract """
        The objective of this course is for students to develop
        proficiency in programming using a high level language.  Topics
        covered include: fundamental programming concepts, program
        testing and debugging, the underlying memory representation of
        data, programming style.  Practical experience of these topics
        is supplied by laboratory programming exercises and assignments.

        Additional Information: This course should be taken by all CSE
        majors, and any other students who have an interest in computing
        or who wish to be extended.  It does not require any prior
        computing knowledge or experience.

        COMP1917 leads directly to COMP1927 and COMP2911, which are the
        pre-requisites for the full range of further computing courses.

        Due to overlapping material, students who complete COMP1917 may
        not enrol in COMP1911 or COMP1921.
    """@en ;
    .

########################################################################

c:COMP1521
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1521" ;
    s:name "Computer Systems Fundamentals" ;
    s:url <https://www.cse.unsw.edu.au/~cs1521/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:coursePrerequisites
        [ unsw:not c:DPST1092 ],
        [ unsw:or c:COMP1511, c:COMP1911, c:COMP1917, c:DPST1091 ] ;
    s:abstract """
        This course provides a programmer's view on how a computer
        system executes programs, manipulates data and communicates.  It
        enables students to become effective programmers in dealing with
        issues of performance, portability, and robustness.  It is
        typically taken in the term after completing COMP1511, but could
        be delayed and taken later.  It serves as a foundation for later
        courses on networks, operating systems, computer architecture
        and compilers, where a deeper understanding of systems-level
        issues is required.

        Topics: Introduction to the systems-level view of computing,
        number representation, machine-level programming, representing
        high-level programs in machine code, memory, input/output,
        system architectures, operating systems, networks,
        parallelism/concurrency, communication/synchronisation.  Labs
        and assignment work in C and machine code.
    """@en ;
    .

########################################################################

c:COMP1531
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1531" ;
    s:name "Software Engineering Fundamentals" ;
    s:url <https://www.cse.unsw.edu.au/~cs1531/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:coursePrerequisites
        [ unsw:not c:SENG1010, c:SENG1020, c:SENG1031 ] ,
        [ unsw:or  c:COMP1511, c:DPST1091, c:COMP1911, c:COMP1917 ] ;
    s:successorOf c:SENG1031 ;
    s:abstract """
        This course provides an introduction to software engineering
        principles: basic software lifecycle concepts, modern
        development methodologies, conceptual modeling and how these
        activities relate to programming.  It also introduces the basic
        notions of team-based project management via conducting a
        project to design, build and deploy a simple web-based
        application.  It is typically taken in the term after completing
        COMP1511, but could be delayed and taken later.  It provides
        essential background for the teamwork and project management
        required in many later courses.


        The goal of this course is to expose the students to: basic
        elements of software engineering: including requirements
        elicitation, analysis and specification, design; construction,
        verification and validation, deployment, and operation and
        maintenance; data modelling; software engineering methodologies,
        processes, measurements, tools and techniques; and Web-based
        system architecture and development practices on Web platforms.
    """@en ;
    .

c:SENG1031
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "SENG1031" ;
    s:name "Software Engineering Workshop 1" ;
    s:url <https://www.cse.unsw.edu.au/~se1031/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    s:succeededBy c:COMP1531 ;
    s:coursePrerequisites
        [ unsw:not c:SENG1010, c:SENG1020 ] ;
    s:abstract """
        The Software Engineering Workshop is a series of courses that
        span the first three years of the Software Engineering program.
        The course series will provide an opportunity to work in small
        teams on substantial, realistic projects, covering most phases
        of the software production life cycle.  The SE Workshop stream
        also provides an opportunity to apply the techniques and methods
        covered in other courses of the course.  Under guidance from
        staff, the intention of this series is to enable students to
        learn by reflective practice.  Whatever steps are taken students
        should become aware of what they are doing, and reflect on the
        consequences.  Each course in the series will involve group
        project work, system development, presentations, and
        reporting.  This is the first course in the series and will
        contain:

        - a discussion of the importance to reliable system
          implementation of a formal description of functional and
          non-functional requirements;
        - an introduction to graphical tools for describing such
          requirements;
        - an introduction to the software process and to a number of the
          software engineering practices to be adopted throughout the
          series;
        - the formation of the first set of small groups;
        - a number of exercises to develop group skills.

        Each group will complete a domain analysis and a requirements
        analysis for a project.  Each group will: examine similar
        systems; interview users or potential users of the system;
        develop a requirements document; validate the requirements by
        prototyping.
    """@en ;
    .

########################################################################

c:COMP1911
    a s:Course ;
    s:provider unsw:COMPSC ;
    s:courseCode "COMP1911" ;
    s:name "Computing 1A" ;
    s:url <https://www.cse.unsw.edu.au/~se1031/> ;
    ps:numberOfCredits 6 ;
    unsw:career unsw:UGRD ;
    # s:successorOf ... ;
    # s:coursePrerequisites  "Enrolment in a non-CSE program." ;
    s:abstract """
        The objective of this course is for students to develop
        proficiency in programming using a high level language.  Topics
        covered include: fundamental programming concepts, program
        testing and debugging, the underlying memory representation of
        data, programming style.  Practical experience of these topics
        is supplied by laboratory programming exercises and assignments.

        Additional Information: This course is designed for
        non-computing majors who want a solid introduction to
        programming, with the aim of using computers as part of some
        other discipline.

        Electrical Engineering, Telecommunications, Mechanical
        Engineering, and Mechatronic Engineering specify this as the
        standard first year computing course.  COMP1911 and the
        following course COMP1921 are gentle courses and combined are
        roughly equivalent to the single course COMP1917.

        Students with an interest in computing or who wish to be
        extended might want to consider COMP1917 as an alternative,
        especially if they plan to study a more computing as part of
        their future study.  Students from all degrees are permitted to
        take 1917 instead of 1911, and 1927 instead of 1921.
    """@en ;
    .

########################################################################

unsw:COMPSC
    a s:CollegeOrUniversity ;
    s:name "School of Computer Science and Engineering" ;
    s:alternateName "CSE" ;
    .

unsw:ENG
    a s:CollegeOrUniversity ;
    s:name "Faculty of Engineering" ;
    s:department unsw:COMPSC ;
    .

unsw:UNSW
    a s:CollegeOrUniversity ;
    s:name "The University of New South Wales, Australia" ;
    s:alternateName "UNSW" ;
    s:department unsw:ENG ;
    .

########################################################################
