========================================================================
			The CSE Course Data-set
========================================================================

:Build (trunk):
   .. image::   https://gitlab.com/jashankj/cse-courses/badges/trunk/pipeline.svg
      :target:  https://gitlab.com/jashankj/cse-courses/commits/trunk
      :alt:     pipeline status for trunk
:Maintainers:
   - Jashank Jeremy


.. WARNING:: This is not authoritative!

   This data is not an authoritative source
   on courses taught at CSE or at UNSW.
   Do not use it to make enrolment decisions.
   Do not assume this data,
   or any of my opinions
   that may have leaked in,
   are in any way representative of UNSW.
   See the UNSW Handbook for authoritative data.


.. WARNING:: This is not guaranteed to be correct!

   Assume information here is best-effort.
   It's almost certainly riddled with errors.


.. WARNING:: This is not complete!

   Despite the appearance of a comprehensive listing,
   including a range of exotic courses
   that otherwise seem lost to time,
   this is by no means a complete list.
   I'm working to expand this data.

   (No, not by creating courses.)

   ((Yet.))


Once upon a time,
I wrote a (now infamous) page on my wiki ---
<https://wiki.jashankj.space/CSE/Courses/> ---
giving a somewhat comprehensive list of courses
that seem to have been taught at CSE
at some point or other.
One of my main regrets about that page is
that it is entirely unstructured data,
even though there is all manner of
useful semantic information embedded within.
So, to remedy that, I went full semantic ...

This repository contains
a portion of the `UNSW Handbook`_,
specific to courses taught at CSE_,
encoded in the Turtle_ serialisation of RDF_.
This effectively gives a weird database,
capturing a collection of data
in a machine-readable way.
My eventual goal is
to find or build a tool
to present or browse this data.

There are three key files here:

- ``courses.ttl``,
  a Turtle file comprising the bulk of course data,
  referred to by prefix ``c:``; and

- ``offerings.ttl``,
  a Turtle file listing course offering data,
  referred to by prefix ``o:``; and

- ``handbook.ttl``,
  a Turtle file defining auxiliary schemata,
  referred to by prefix ``unsw:``,
  needed to model the UNSW Handbook.


.. _`UNSW Handbook`: https://www.handbook.unsw.edu.au/
.. _CSE:             https://www.cse.unsw.edu.au/
.. _Turtle:          https://www.w3.org/TR/turtle/
.. _RDF:             https://www.w3.org/TR/rdf11-concepts/
